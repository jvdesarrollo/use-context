import { useState } from 'react';
import './App.css';
import ComponentA from './components/ComponentA';
// import ComponentB from './components/ComponentB';
import { MyCounter } from './components/MyCounter';
import CounterContexProvider from './context/CounterContext';

function App() {
  return (
    <CounterContexProvider>
      <div className="App">
        <h1>Context API</h1>
        <MyCounter />
        <ComponentA />
      </div>
    </CounterContexProvider>
  );
}

export default App;
