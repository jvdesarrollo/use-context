import { useState, createContext, useContext } from 'react';

const CounterContex = createContext();
//Custom Hook
export const useCounter = () => useContext(CounterContex);

function CounterContexProvider(props) {
  const [count, setCount] = useState(0);
  const increaseCount = () => {
    setCount(count + 1);
  };
  const decreaseCount = () => {
    setCount(count - 1);
  };
  const value = { count, increaseCount, decreaseCount };

  return (
    <CounterContex.Provider value={value}>
      {props.children}
    </CounterContex.Provider>
  );
}
export default CounterContexProvider;
