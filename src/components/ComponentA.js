import React from 'react';
import { useCounter } from '../context/CounterContext';
import ComponentB from './ComponentB';
/**
 * @author
 * @function ComponentA
 **/

const ComponentA = () => {
  const { count } = useCounter();
  return (
    <div style={{ background: 'wheat' }}>
      <p>Count: {count}</p>
      <ComponentB/>
    </div>
  );
};
export default ComponentA;
