import React, { useState } from 'react';
import ComponentC from './ComponentC';
import { useCounter } from '../context/CounterContext';

/**
 * @author
 * @function ComponentB
 **/

const ComponentB = () => {
  const { increaseCount } = useCounter();
  return (
     <div style={{ background: 'red' }}>
      <button onClick={increaseCount}>Increase Count</button>
    </div>
  );
};
export default ComponentB;
