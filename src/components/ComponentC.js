import React from 'react';

/**
 * @author
 * @function ComponentC
 **/

const ComponentC = (props) => {
  const { name } = props;
  return (
    <div>
      <h3>ComponentC: {name}</h3>
    </div>
  );
};

export default ComponentC;
