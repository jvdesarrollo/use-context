import React, { useContext } from 'react';
import { CounterContex } from '../context/CounterContext';
import { useCounter } from '../context/CounterContext';
// import
/**
 * @author
 * @function MyCounter
 **/

export const MyCounter = (props) => {
  const { count, increaseCount, decreaseCount } = useCounter();
  return (
    <div>
      <h3>Counter Component</h3>
      <p>Count is: {count}</p>
      <button onClick={increaseCount}>Increase Count</button>
      <button onClick={decreaseCount}>Decrease Count</button>
    </div>
  );
};
